//
//  Moto.swift
//  CanadaRally
//
//  Created by Antariksh Gosain on 2019-10-19.
//  Copyright © 2019 Antariksh Gosain. All rights reserved.
//

import Foundation

class Moto : Vehicle {
    
    var hasSideCar : Bool = false ;
    
    init (hasSideCar : Bool , name : String , number : String , maxSpeed : Double , weight : Int , fuel : Int , performance : Double) {
           self.hasSideCar = hasSideCar
        super.init(name: name, number: number, maxSpeed: maxSpeed, weight: weight, fuel: fuel, performance: performance)
    }
    
    override func printVehicle() {
        if (self.hasSideCar) {
            print ("\u{1F6F5} \(self.name) -> speed max = \(self.maxSpeed) km/h, weight \(self.weight) kg. Moto, with sidecar")
        } else {
            print ("\u{1F3CD} \(self.name) -> speed max = \(self.maxSpeed) km/h, weight \(self.weight) kg. Moto")
        }
    }
    
    func getHasSideCar() -> Bool {
        return self.hasSideCar
    }
    
}

