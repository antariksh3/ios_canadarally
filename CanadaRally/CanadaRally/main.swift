//
//  main.swift
//  CanadaRally
//
//  Created by Antariksh Gosain on 2019-10-18.
//  Copyright © 2019 Antariksh Gosain. All rights reserved.
//

import Foundation

//System Variables
var debugMode : Bool = true

var defaultVehicleName : String = "Anonym"
var defaultFuel : Int = 0
var defaultMaxSpeed : Double = 130
var defaultWeight : Int = 1000

func getCurrentTimeString() -> String {
    let time = Date()
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyyMMdd-HHmmss"
    let formatteddate = formatter.string(from: time)
    return formatteddate
}

func calculatePerformance ( speed : Double , weight : Int ) -> Double {
    let performance : Double = speed * 100 / Double(weight)
    return performance
}

if ( debugMode ) {
    //TODO
    //print("DEFAULT NUM PLATE : \(getCurrentTimeString())")
}

//Initializing Vehicles
//Initializing Cars
var car1 : Car = Car(carType: CarTypeEmum.racing.rawValue , name: "Ferrari F1", number: "FER 001", maxSpeed: 240, weight: 2500, fuel: 60, performance: calculatePerformance(speed: 240, weight: 2500))
var car2 : Car = Car(carType: CarTypeEmum.racing.rawValue , name: "Audi A4", number: "KLO 022", maxSpeed: 220, weight: 2200, fuel: 50, performance: calculatePerformance(speed: 220, weight: 2200))
var car3 : Car = Car(carType: CarTypeEmum.touring.rawValue , name: "Nissan T90", number: "JMS 007", maxSpeed: 150, weight: 2300, fuel: 80, performance: calculatePerformance(speed: 150, weight: 2300))
var car4 : Car = Car(carType: CarTypeEmum.racing.rawValue , name: "BMW X9", number: "PQR 987", maxSpeed: 300, weight: 3600, fuel: 100, performance: calculatePerformance(speed: 300, weight: 3600))
var car5 : Car = Car(carType: CarTypeEmum.touring.rawValue , name: "Volvo VX1", number: "VOL 199", maxSpeed: 120, weight: 4000, fuel: 100, performance: calculatePerformance(speed: 120, weight: 4000))

//Initializing Bikes
var moto1 : Moto = Moto(hasSideCar: false, name: "Kawasaki Ninja", number: "KWA 6X9", maxSpeed: 180, weight: 700, fuel: 50, performance: calculatePerformance(speed: 180, weight: 700))
var moto2 : Moto = Moto(hasSideCar: true, name: "1942 BSA", number: "BSA 942", maxSpeed: 80, weight: 870, fuel: 50, performance: calculatePerformance(speed: 80, weight: 870))
var moto3 : Moto = Moto(hasSideCar: false, name: "Hayabusa", number: "HHY 222", maxSpeed: 200, weight: 700, fuel: 30, performance: calculatePerformance(speed: 200, weight: 700))
var moto4 : Moto = Moto(hasSideCar: true, name: "Harley Davidson HD-44", number: "KAR 642", maxSpeed: 140, weight: 650, fuel: 65, performance: calculatePerformance(speed: 140, weight: 650))
var moto5 : Moto = Moto(hasSideCar: true, name: "Harley Davidson HARD-08", number: "CPT 881", maxSpeed: 150, weight: 550, fuel: 45, performance: calculatePerformance(speed: 120, weight: 550))
var moto6 : Moto = Moto(hasSideCar: false, name: "RTX 1000", number: "XIO 191", maxSpeed: 200, weight: 550, fuel: 40, performance: calculatePerformance(speed: 200, weight: 550))
var moto7 : Moto = Moto(hasSideCar: false, name: "Freeway", number: "FREE 0000", maxSpeed: 175, weight: 400, fuel: 35, performance: calculatePerformance(speed: 175, weight: 400))

//Initializing Set of Vehicles
var vehicles = Set<Vehicle>()

//Adding initialized Cars into the Vehicles Set
vehicles.insert(car1);
vehicles.insert(car2);
vehicles.insert(car3);
vehicles.insert(car4);
vehicles.insert(car5);

//Adding initialized Motos into the Veicals Set
vehicles.insert(moto1);
vehicles.insert(moto2);
vehicles.insert(moto3);
vehicles.insert(moto4);
vehicles.insert(moto5);
vehicles.insert(moto6);
vehicles.insert(moto7);

var choice : Int = 1

func displayAllVehicles() {
    if ( vehicles.count <= 0 ) {
        print("No Entries Present, Please Add Vehicles")
        //function ends if no Vehicles are found
        return
    } else {
       print("Here are the Details of all Existing Vehicles")
    }
    for vehi in vehicles {
        //All the Vehicles are printed
        vehi.printVehicle()
        if ( debugMode ) {
            vehi.printExtraDetails()
        }
    }
}

func displayAllCars() {
    var hasCars : Bool = false
    for vehi in vehicles {
        if ( vehi is Car ) {
            hasCars = true
            break
        }
    }
    if ( hasCars ) {
        print("Here is the List of All Available Cars")
        //Displays all Cars
        for vehi in vehicles {
            if ( vehi is Car ) {
                vehi.printVehicle()
                if ( debugMode ) {
                    vehi.printExtraDetails()
                }
            }
        }
    } else {
        print("No Cars Found! Please Add Them")
    }
}

func displayAllBikes() {
    var hasBikes : Bool = false
    for vehi in vehicles {
        if ( vehi is Moto ) {
            hasBikes = true
            break
        }
    }
    if ( hasBikes ) {
        print("Here is the List of All Available Bikes")
        //Displays all Motos
        for vehi in vehicles {
            if ( vehi is Moto ) {
                vehi.printVehicle()
                if ( debugMode ) {
                    vehi.printExtraDetails()
                }
            }
        }
    } else {
        print("No Bikes Found! Please Add Them")
    }

}

//This is a Utility Method Used to read Int form User
func readInt ( fullMessage line : String , smallMessage word : String ) -> Int {
    var result : Int? = -1
    var showError : Bool = false
    print("\(line)")
    repeat {
        if ( showError ) {
            print("Please Enter A Valid Int for \(word)")
        }
        if let input = Int((readLine() ?? "-1")! ) {
            result = input
        }
        showError = true
    } while ( result! < 0 )
    return result!
}

//This is a Utility Method Used to read a Positive Int form User
func readPositiveInt ( fullMessage line : String , smallMessage word : String ) -> Int {
    var result : Int? = -1
    var showError : Bool = false
    print("\(line)")
    repeat {
        if ( showError ) {
            print("Please Enter A Valid Int (Greater Than Zero) for \(word)")
        }
        if let input = Int((readLine() ?? "-1")! ) {
            result = input
        }
        showError = true
    } while ( result! < 1 )
    return result!
}

//This is a Utility Method Used to read Double form User
func readDouble ( fullMessage line : String , smallMessage word : String ) -> Double {
    var result : Double? = -1
    var showError : Bool = false
    print("\(line)")
    repeat {
        if ( showError ) {
            print("Please Enter A Valid Double for \(word)")
        }
        if let input = Double((readLine() ?? "-1")! ) {
            result = input
        }
        showError = true
    } while ( result! < 0 )
    return result!
}

//This is a Utility Method Used to read a Positive Double form User
func readPositiveDouble ( fullMessage line : String , smallMessage word : String ) -> Double {
    var result : Double? = -1
    var showError : Bool = false
    print("\(line)")
    repeat {
        if ( showError ) {
            print("Please Enter A Valid Double (Greater Than Zero) for \(word)")
        }
        if let input = Double((readLine() ?? "-1")! ) {
            result = input
        }
        showError = true
    } while ( result! < 0.0001 )
    return result!
}

//This is a Utility Method Used to read String form User
func readString ( fullMessage line : String , smallMessage word : String ) -> String {
    var result : String = "default"
    print("\(line)")
    repeat {
        if ( result == "" ) {
            print("Please Enter A Valid String for \(word)")
        }
        if let input = (readLine())  {
            result = input
        }
    } while ( result == "" )
    return result
}

func isNumberAvailable ( forNumber num : String ) -> Bool {
    for vehi in vehicles {
        if ( vehi.getNumber() == num ) {
            //means that this number plate exists
            return false
        }
    }
    //means that his number is new
    return true
}

func getVehicleByNumber ( forNumber num : String ) -> Vehicle {
    for vehi in vehicles {
        if ( vehi.getNumber() == num ) {
            //Vehicles with given number plate is found and is being returned
            return vehi
        }
    }
    //Code will never reach here as the getVehicleByNumber() will only be called when isNumberAvailable() function return true. Hence, there below return statement is Dead Code
    return Vehicle(VehicleNumber: getCurrentTimeString() , performance: 0.0 )
}

func addCar() {
    var isNumberValid : Bool = false
    var VehicleNumber : String = ""
    repeat {
        VehicleNumber = readString( fullMessage : "Enter The Number Of Vehicle" , smallMessage : "Vehicle Number")
        isNumberValid = isNumberAvailable(forNumber: VehicleNumber)
        if ( !isNumberValid ) {
            print("Number Already Exists! Please ReCheck And Then")
        }
    } while (!isNumberValid)
    //Read Input form User
    let name : String = readString(fullMessage: "Enter the Car's Name", smallMessage: "Car's Name")
    let maxSpeed : Double = readPositiveDouble(fullMessage: "Enter Car's Max Speed", smallMessage: "Max Speed")
    let weight : Int = readPositiveInt(fullMessage: "Enter Car's Weight", smallMessage: "Weight")
    let fuel : Int = readInt(fullMessage: "Enter Car's Fuel", smallMessage: "Fuel")
    var carTypeStr : String = ""
    var carTypeNum : Int = 0
    repeat {
        let tempNum = readInt(fullMessage: "Whats the type of this Car? \n1. Racing \n2. Touring \nPlease Enter your choice Below", smallMessage: "Car Type (i.e. 1 or 2)")
        carTypeNum = tempNum
    } while ( carTypeNum < 1 || carTypeNum > 2 ) //to make sure that user selects either 1 or 2
    
    if ( carTypeNum == 1 ) {
        carTypeStr = CarTypeEmum.racing.rawValue
    }
    if ( carTypeNum == 2 ) {
        carTypeStr = CarTypeEmum.touring.rawValue
    }
    
    //Calling the Constructor
    let newCar : Car = Car(carType: carTypeStr, name: name, number: VehicleNumber, maxSpeed: maxSpeed, weight: weight, fuel: fuel, performance: calculatePerformance(speed: maxSpeed, weight: weight))
    //Adding the new Car into Vehicles Set
    vehicles.insert(newCar)
    
    if ( debugMode ) {
        print("\nCar Inserted with following details")
        print("Name=\(name) Number=\(VehicleNumber) Type=\(carTypeStr) MaxSpeed=\(maxSpeed) Weight=\(weight) Fuel=\(fuel)")
    }
}

func addBike() {
    var isNumberValid : Bool = false
    var VehicleNumber : String = ""
    repeat {
        VehicleNumber = readString( fullMessage : "Enter The Number Of Vehicle" , smallMessage : "Vehicle Number")
        isNumberValid = isNumberAvailable(forNumber: VehicleNumber)
        if ( !isNumberValid ) {
            print("Number Already Exists! Please ReCheck And Then")
        }
    } while (!isNumberValid)
 
    let name : String = readString(fullMessage: "Enter the Bike's Name", smallMessage: "Bike's Name")
    let maxSpeed : Double = readPositiveDouble(fullMessage: "Enter Bike's Max Speed", smallMessage: "Max Speed")
    let weight : Int = readPositiveInt(fullMessage: "Enter Bike's Weight", smallMessage: "Weight")
    let fuel : Int = readInt(fullMessage: "Enter Bike's Fuel", smallMessage: "Fuel")
    var bikeHasSideCar : Bool = false
    var typeNum : Int = 0
    repeat {
        let tempNum = readInt(fullMessage: "Does This Bike Has A Side Car Attached? \n1. Yes \n2. No \nPlease Enter your choice Below", smallMessage: "your Choice (i.e. 1 for 'Yes, Bike Has A Side Car' and 2 for 'No, Bike Does NOT Have A Side Car')")
        typeNum = tempNum
    } while ( typeNum < 1 || typeNum > 2 ) //to make sure that user selects either 1 or 2
    
    if ( typeNum == 1 ) {
        bikeHasSideCar = true
    }
    if ( typeNum == 2 ) {
        bikeHasSideCar = false
    }
    
    //Calling the Constructor
    let newBike : Moto = Moto(hasSideCar: bikeHasSideCar, name: name, number: VehicleNumber, maxSpeed: maxSpeed, weight: weight, fuel: fuel, performance: calculatePerformance(speed: maxSpeed, weight: weight))
    //Adding new Bike into the Vehicles Set
    vehicles.insert(newBike)
    
    if ( debugMode ) {
        print("\nMoto Inserted with following details")
        print("Name=\(name) Number=\(VehicleNumber) SideCar=\(bikeHasSideCar) MaxSpeed=\(maxSpeed) Weight=\(weight) Fuel=\(fuel)")
    }
}

func addDefaultVehicle() {
    var isNumberValid : Bool = false
    var VehicleNumber : String = ""
    repeat {
        VehicleNumber = getCurrentTimeString()
        isNumberValid = isNumberAvailable(forNumber: VehicleNumber)
        if ( !isNumberValid ) {
            print("\(VehicleNumber) already taken, looking for new number")
        }
    } while (!isNumberValid)
    
    //Calling Constructor for new Vehicle
    let newVehicle : Vehicle = Vehicle(VehicleNumber: VehicleNumber , performance: calculatePerformance(speed: 130, weight: 1000))
    
    //Adding Vehiacl into the Vehicles Set
    vehicles.insert(newVehicle)
    
    if ( debugMode ) {
        print("\nVehicle Inserted with following details")
        print("Number=\(VehicleNumber)")
    }
}

func addNewVehicle() {
    var VehicleTypeChoice : Int = 0
    print("Which Type Of Vehicle Do You Want To Add?")
    print("1. Car")
    print("2. Bike")
    print("3. Default Vehicle")
    print("Enter Your Choice")
    
    if let input = Int(readLine()!) {
        VehicleTypeChoice = input
    }
    
    switch VehicleTypeChoice {
        case 1 :
            addCar()
            break ;
        case 2 :
            addBike()
            break ;
        case 3 :
            addDefaultVehicle()
            break ;
        default :
            print("Please Enter A Valid Input and Try Again")
        
    }
}

func removeExistingVehicle() {
    let numberToRemove : String = readString(fullMessage: "Enter the Vehicle's Number You Want to Remove", smallMessage: "Vehicle's Number")
    if ( !isNumberAvailable(forNumber: numberToRemove) ) {
        let VehicleToRemove = getVehicleByNumber(forNumber: numberToRemove)
        VehicleToRemove.printVehicle()
        print("Vehicle Removed!")
        //removing Vehicle
        vehicles.remove(VehicleToRemove)
    } else {
        print("No Vehicle Found Against Number : \(numberToRemove)")
        print("Please ReCheck And Try Again")
    }
}

func compareVehicles() {
    
    var choice : String = "y"
    var comparisonList = [Vehicle]()
    
    repeat {
        let VehicleNumber : String = readString(fullMessage: "Enter The Vehicle Number To Add Vehicle in Comparison List", smallMessage: "Vehicle Number")
        if ( !isNumberAvailable(forNumber: VehicleNumber) ) {
            let vehi : Vehicle = getVehicleByNumber(forNumber: VehicleNumber)
            if ( comparisonList.contains(vehi) ) {
                print("Vehicle with Number: '\(VehicleNumber)' is already added for Comparison. Please Add Another Vehicle Number.")
            } else {
                comparisonList.append(vehi)
                print("Vehicle Added! Name: \(vehi.getName()) Number: \(vehi.getNumber())")
            }
            
        } else {
            print("No Vehicle Found With Number: \(VehicleNumber)")
        }
        let input = readString(fullMessage: "Do You Want To Add Another Vehicle In The Comparison List? \nEnter 'y' or 'n'", smallMessage: "Choice")
        if ( input == "y" || input == "Y" ) {
            choice = input
        } else {
            choice = "n"
        }
    } while ( choice == "y" )
    
    if ( comparisonList.count < 2 ) {
        print("Comparison Can't Take Place As There Should Be Atleast 2 Vehicles Up For Comparison")
        return
    } else {
        //Printing the list after comparison based on Performance
        print("Comparing the following \(comparisonList.count) Vehicles")
        for veh in comparisonList {
            veh.printVehicle()
            veh.printExtraDetails()
        }
    }
    
    var sortedList = [Vehicle]()
    //coparing the list based on performance
    sortedList = comparisonList.sorted(by: { $0.performance > $1.performance })
    
    print("\nSORTED LIST :-")
    var rank : Int = 1
    for vehic in sortedList {
        if ( rank == 1 ) {
            print("RANK \(rank) \u{1F947}") //emoji for gold medal
        }
        if ( rank == 2 ) {
            print("RANK \(rank) \u{1F948}") //emoji for silver medal
        }
        if ( rank == 3 ) {
            print("RANK \(rank) \u{1F949}") //emoji for bronze medal
        }
        if ( rank >= 4 ) {
            print("RANK \(rank)") // no medal emoji for rank >= 4
        }
        rank += 1
        vehic.printVehicle()
        vehic.printExtraDetails()
    }
    
}

extension Bool {
    //this extension make the XOR operation possible for Bools
    static func ^ (left: Bool, right: Bool) -> Bool {
        return left != right
    }
}

func check ( for raceList : [Vehicle] ) -> Bool {
    var fourWheelerPresent : Bool = false
    var twoWheelerPresent : Bool = false
    // Initially we have not checked the list, so both bools are false
    for vehi in raceList {
        if ( vehi is Car ) {
            //four wheeler present
            fourWheelerPresent = true
        }
        if let motoSideCar = vehi as? Moto {
            if ( motoSideCar.getHasSideCar() ) {
                //four wheeler present
                fourWheelerPresent = true
            } else {
                //two wheeler present
                twoWheelerPresent = true
            }
        }
    }
    //  XOR Operation Between A and B
    //        A    B    A^B
    //        1    1     0
    //        1    0     1
    //        0    1     1
    //        0    0     0
    // Because Race can only take place if the list has only four-wheelers or only two wheelers
    return twoWheelerPresent ^ fourWheelerPresent
}

func organizeGrandPrixRace() {
    
    //in order to clear up the console
    for _ in 1...100 {
        print("")
    }
    print("Welcome To The GRAND PRIX Race! \u{1F3C1}")
    print("\nRULES")
    print("1. Race Can Only Take Place When There Are At Least 2 Ready To Race")
    print("2. Vehicles Participating In The Race Should Have Same Number Of Wheels ")
    print("Which Means That Touring Car, Racing Car & Moto With Side Car Can Race Together (as they all have atleast 4 wheels)")
    print("And Moto Without Side Car Can Race Among Themselves (as they have 2 wheels)\n\n\n\n")
    var choice : String = "y"
    var raceList = [Vehicle]()
    
    repeat {
        let VehicleNumber : String = readString(fullMessage: "Enter The Vehicle Number To Add Vehicle in List for GRAND PRIX Race", smallMessage: "Vehicle Number")
        if ( !isNumberAvailable(forNumber: VehicleNumber) ) {
            let vehi : Vehicle = getVehicleByNumber(forNumber: VehicleNumber)
            if ( raceList.contains(vehi) ) {
                print("Vehicle with Number: '\(VehicleNumber)' is already added for the GRAND PRIX Race. Please Add Another Vehicle Number.")
            } else {
                raceList.append(vehi)
                print("Vehicle Added! Name: \(vehi.getName()) Number: \(vehi.getNumber())")
            }
            
        } else {
            print("No Vehicle Found With Number: \(VehicleNumber)")
        }
        let input = readString(fullMessage: "Do You Want To Add Another Vehicle In The List for GRAND PRIX Race? \nEnter 'y' or 'n'", smallMessage: "Choice")
        if ( input == "y" || input == "Y" ) {
            choice = input
        } else {
            choice = "n"
        }
    } while ( choice == "y" )
    
    //Race Can Only Take Place if there are atleast 2 Vehicles Up For Competetion
    if ( raceList.count < 2 ) {
        print("Race Can't Take Place As There Should Be Atleast 2 Vehicles Up For Competetion")
        return;
    } else {
        print("Race Begins for the following \(raceList.count) Vehicles")
        for veh in raceList {
            veh.printVehicle()
            veh.printExtraDetails()
        }
    }
    
    //check() method is called in order to verify if race can take place
    let isRacePossible = check(for: raceList)
    
    //new list for Vehicles that have enough fuel to complete the Race
    var raceCompletedList = [Vehicle]()
    
    if ( isRacePossible ) {
        print("Race is Possible!")
        let turns : Int = readPositiveInt(fullMessage: "Enter The Number Of Turns The Race Track Will Have", smallMessage: "Turns")
        for veh in raceList {
            if ( veh.getFuel() > turns ) {
                raceCompletedList.append(veh)
            }
        }
        if ( raceCompletedList.count > 0 ) {
            var sortedList = [Vehicle]()
            sortedList = raceCompletedList.sorted(by: { $0.performance > $1.performance })
            // First Element in the Sorted List will have the Winner as it will have the most performance points
            let winner : Vehicle = (sortedList.first ?? nil)!
            print("\u{1F3C6}Winner is \(winner.getName())\u{1F3C6}")// trophy emoji
            winner.printVehicle()
            winner.printExtraDetails()
        } else {
            //None of The Vehicles were able to Finish the Race
            print("All The Vehicles Failed To Finish The Rally")
            return
        }
    } else {
        print("Race Can NOT Take Place As All The Vehicles Are NOT Of The Same Type")
        return
    }
    
    //New List for Sorted Veicals Based on Performance
    var sortedList = [Vehicle]()
    //soreted List is assigned Values based on Performance
    sortedList = raceCompletedList.sorted(by: { $0.performance > $1.performance })
    
    print("\nRACE RANKING :-")
    var rank : Int = 1
    for vehic in sortedList {
        if ( rank == 1 ) {
            print("RANK \(rank) \u{1F947}") // emoji for gold medal
        }
        if ( rank == 2 ) {
            print("RANK \(rank) \u{1F948}") // emoji for silver medal
        }
        if ( rank == 3 ) {
            print("RANK \(rank) \u{1F949}") // emoji for bronze medal
        }
        if ( rank >= 4 ) {
            print("RANK \(rank)") // no emoji for rank >= 4
        }
        rank += 1
        vehic.printVehicle()
        vehic.printExtraDetails()
    }
    
}

repeat {
    
    print("\nMENU")
    print("1. Display All Vehicles (All Cars & All Bikes)")
    print("2. Display All Cars (Both Racing and Touring)")
    print("3. Display All Bikes (With and Without SideCar)")
    print("4. Add New Vehicle")
    print("5. Remove Existing Vehicle")
    print("6. Compare Vehicles")
    print("7. Organize Grand Prix Race")
    print("Enter Any Other Number To Exit")
    
    choice = readPositiveInt( fullMessage : "Enter your Choice" , smallMessage : "Choice")
    
    switch (choice) {
        case 1 :
            displayAllVehicles()
            break;
        case 2 :
            displayAllCars()
            break;
        case 3 :
            displayAllBikes()
            break;
        case 4 :
            addNewVehicle()
            break;
        case 5 :
            removeExistingVehicle()
            break;
        case 6 :
            compareVehicles()
            break;
        case 7 :
            organizeGrandPrixRace()
            break;
        default :
            print("CanadaRally : Program will exit now. Thanks & see you soon")
            //default called
    }
} while ( choice > 0 && choice < 8 ) // code will keep iterating unless a wrong number is Entered
