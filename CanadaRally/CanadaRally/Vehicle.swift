//
//  Vehical.swift
//  CanadaRally
//
//  Created by Antariksh Gosain on 2019-10-18.
//  Copyright © 2019 Antariksh Gosain. All rights reserved.
//

import Foundation

class Vehicle :  Hashable , VehicleProtocol {
    
    var hashValue : Int {
        return self.number.hashValue
    }
    
    static func == (lhs: Vehicle, rhs: Vehicle) -> Bool {
        return lhs.number == rhs.number
    }
    
    //Declaring properties of Vehical Class
    var name : String
    var number : String
    var maxSpeed : Double
    var weight : Int
    var fuel : Int
    
    var performance : Double
    
    //The code will show a lot more print statements when the debugMode boolean is se to true
    var debugMode : Bool = true
    
    //Defining the method body of the Protocol's Method
    func run() -> String {
        return "This Vehicle is now running"
    }
    
    
    //constructor with arguments that will create a Vehical object with passed values
    init ( name : String , number : String , maxSpeed : Double , weight : Int , fuel : Int , performance : Double) {
        self.name = name
        self.number = number
        self.maxSpeed = maxSpeed
        self.weight = weight
        self.fuel = fuel
        self.performance = performance
    }
    
    //constructor without details (only the Vehical's number) which will assign default values to the Vehical Object
    init ( VehicleNumber number : String , performance : Double) {
        self.name = "Anonym"
        self.maxSpeed = 130
        self.weight = 1000
        self.fuel = 0
        self.number = number
        self.performance = performance
    }
    
    //this method will print the Vehical Details when called upon
    func printVehicle () {
        print ("\(self.name) -> speed max = \(self.maxSpeed) km/h, weight \(self.weight) kg")
    }
    
    func printExtraDetails () {
        print("Number = \(self.number), Performance = \(self.performance) ")
    }
    
    //this function prints the noise that the Vehical makes
    func makeNoise() {
        print("Vrooooom")
    }
    
    func getCurrentDate() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMddHHmmss"
        return formatter.string(for: date)!
    }
    
    //Getters
    func getName() -> String { return self.name }
    func getNumber() -> String { return self.number }
    func getMaxSpeed() -> Double { return self.maxSpeed }
    func getWeight() -> Int { return self.weight }
    func getFuel() -> Int { return self.fuel }
}

