//
//  CarTypeEnum.swift
//  CanadaRally
//
//  Created by Antariksh Gosain on 2019-10-19.
//  Copyright © 2019 Antariksh Gosain. All rights reserved.
//

import Foundation


enum CarTypeEmum : String {
    case racing
    case touring
}
