//
//  VehicalProtocol.swift
//  CanadaRally
//
//  Created by Antariksh Gosain on 2019-10-18.
//  Copyright © 2019 Antariksh Gosain. All rights reserved.
//

import Foundation

protocol VehicleProtocol {
    
    func run() -> String
    func getName() -> String
    func getNumber() -> String
    func getMaxSpeed() -> Double
    func getWeight() -> Int
    func getFuel() -> Int
    func printVehicle()
    func printExtraDetails()
}

