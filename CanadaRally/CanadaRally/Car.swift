//
//  Car.swift
//  CanadaRally
//
//  Created by Antariksh Gosain on 2019-10-19.
//  Copyright © 2019 Antariksh Gosain. All rights reserved.
//

import Foundation

class Car : Vehicle {
    
    var carCategory : String
    
    init (carType : String , name : String , number : String , maxSpeed : Double , weight : Int , fuel : Int , performance : Double ) {
        self.carCategory = carType
        super.init(name: name, number: number, maxSpeed: maxSpeed, weight: weight, fuel: fuel, performance: performance)
    }
    
    override func printVehicle() {
        if ( self.carCategory == CarTypeEmum.racing.rawValue || self.carCategory == CarTypeEmum.touring.rawValue ) {
            if ( self.carCategory == CarTypeEmum.racing.rawValue ) {
                print ("\u{1F3CE} \(self.name) -> speed max = \(self.maxSpeed) km/h, weight \(self.weight) kg, car category = \(self.carCategory)")
            } else {
                print ("\u{1F699} \(self.name) -> speed max = \(self.maxSpeed) km/h, weight \(self.weight) kg, car category = \(self.carCategory)")
            }
        } else {
            print ("\(self.name) -> speed max = \(self.maxSpeed) km/h, weight \(self.weight) kg.")
        }
    }
    
    func getCarCategory () -> String {
        return self.carCategory
    }
}


